package com.company;

public class Main {

    public static void main(String[] args) {
	Human one = new Human("Невинская", "Юлия", "Николаевна");
	Human two = new Human("Невинская", "Юлия");

        System.out.println(one.getFullName());
        System.out.println(one.getShortName());
        System.out.println(two.getFullName());
        System.out.println(two.getShortName());
    }
}
