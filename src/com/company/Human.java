package com.company;

public class Human {
    private String firstName;
    private String lastName;
    private String patronymic;

    public Human(String lastName, String firstName){
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Human(String lastName, String firstName, String patronymic){
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
    }

    public String getFullName(){
        String fullName;
        if ((this.lastName!=null)&&(this.firstName!=null)&&(this.patronymic!=null)){
            fullName = this.lastName + " " + this.firstName + " " + this.patronymic;
        } else
            if ((this.lastName!=null)&&(this.firstName!=null)&&(this.patronymic==null)){
                fullName = this.lastName + " " + this.firstName;
            } else fullName = "Неверно введены данные";
            return fullName;
    }

    public String getShortName(){
        String shortName;
        if ((this.lastName!=null)&&(this.firstName!=null)&&(this.patronymic!=null)){
            shortName = this.lastName + " " + this.firstName.substring(0,1) + ". " + this.patronymic.substring(0,1) + ".";
        } else
            if ((this.lastName!=null)&&(this.firstName!=null)&&(this.patronymic==null)){
                shortName = this.lastName + " " + this.firstName.substring(0,1) + ".";
        } else shortName = "Неверно введены данные";
            return shortName;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if ((object == null) || (object.getClass() != this.getClass())) {
            return false;
        }
        Human human = (Human) object;
        return ((firstName == human.firstName)
                || (firstName != null && firstName.equals(human.getCenter())))
                && ((lastName == human.lastName
                || (lastName != null && lastName.equals(human.getCenter()))))
                && ((patronymic == human.patronymic
                || (patronymic != null && patronymic.equals(human.getCenter()))))
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((patronymic == null) ? 0 : patronymic.hashCode());
        return result;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Human:\nfirst name:" + firstName
                + " last name: " + lastName
                + " patronymic: " + patronymic
                + ".";
    }
}